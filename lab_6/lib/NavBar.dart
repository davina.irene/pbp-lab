import 'package:flutter/material.dart';
import 'HomePagePengajar.dart';

class NavBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final hoverColor = Colors.purple;

    return Drawer(
      child: Material(
   
        child: ListView(
        padding: EdgeInsets.zero,

        children: [
          UserAccountsDrawerHeader(
            accountName: const Text('Dummy name'),
            accountEmail: const Text('dummy123@gmail.com'),
            currentAccountPicture: CircleAvatar(
              child: ClipOval(
                child: Image.asset(
                  "assets/profile.png",
                  fit: BoxFit.cover,
                ),
              ),
            ),

            decoration: const BoxDecoration(
              color: Colors.purple,
            ),
          ),

          ListTile(
            leading: const Icon(Icons.home),
            title: const Text('Kuis Kuy'),
            hoverColor: hoverColor,
            onTap: () {},
          ),


          ListTile(
            leading: const Icon(Icons.logout),
            title: const Text('Keluar'),
            hoverColor: hoverColor,
            onTap: () {},
          ),


          ListTile(
            title: const Text('Home Page Pengajar'),
            hoverColor: hoverColor,
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const HomePagePengajar()));
            },
          ),


        ],
      ),
    ));
  }
}
