import 'package:flutter/material.dart';

class HomePagePengajar extends StatelessWidget {
  const HomePagePengajar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Home Pengajar"),
      ),
      body: Container(
        height: 600,
        width: double.infinity,
        decoration: const BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Colors.purple, Colors.lightBlue])),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 250,
              height: 200,
              decoration: BoxDecoration(
                color: const Color.fromRGBO(211, 215, 248, 0.6),
                borderRadius: BorderRadius.circular(18),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.06),
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: const Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              child: ListView(
                padding: const EdgeInsets.only(left: 40, right: 40, top: 35),
                children: <Widget>[
                  Container(
                    height: 60,
                    // padding: EdgeInsets.only(bottom: 30, top: 30),
                    child: ElevatedButton(
                      child: const Text('Daftar Kuis'),
                      onPressed: () {
                        // print(kodeController.text);
                      },
                    ),
                  ),
                  Container(
                    height: 60,
                    // padding: EdgeInsets.only(bottom: 30, top: 30),
                    child: ElevatedButton(
                      child: const Text('Kumpulan Kuis'),
                      onPressed: () {
                        // print(kodeController.text);
                      },
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
