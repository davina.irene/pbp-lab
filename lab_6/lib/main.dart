import 'package:flutter/material.dart';
import 'package:lab_6/NavBar.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController kodeController = TextEditingController();
  String kode = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavBar(),
      appBar: AppBar(
        title: const Text('Home page'),
        backgroundColor: Colors.deepPurple,
      ),
      body: Container(
          height: 600,
          width: double.infinity,
          decoration: const BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Colors.purple, Colors.lightBlue])),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              // flying box
              Container(
                width: 300,
                height: 300,
                decoration: BoxDecoration(
                  color: const Color.fromRGBO(211, 215, 248, 0.6),
                  borderRadius: BorderRadius.circular(18),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.06),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: const Offset(0, 3), // changes position of shadow
                    ),
                  ],
                ),

                // isi dari flying box
                child: ListView(
                  children: <Widget>[
                    Container(
                      padding: const EdgeInsets.only(top: 60),
                      alignment: Alignment.center,
                      child: const Text("Hi, Dummy",
                          style: TextStyle(
                              fontFamily: 'Satisfy-Regular',
                              fontSize: 45,
                              color: Colors.purple)),
                    ),


                    Container(
                      padding: const EdgeInsets.only(left: 40, right: 40, top: 15),
                      child: TextField(
                        controller: kodeController,
                        decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            hintText: 'Masukkan kode kuis disini',
                            hintStyle: TextStyle(color: Colors.deepPurple),
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 5, horizontal: 15)),
                      ),
                    ),


                    Container(
                      height: 60,
                      padding: const EdgeInsets.only(top: 20, left: 70, right: 70),
                      child: ElevatedButton(
                        child: const Text('Enter'),
                        onPressed: () {
                          print(kodeController.text);
                        },
                      ),
                    )
                  ],
                ),
              )
            ],
          )

         
          ),
    );
  }
}
