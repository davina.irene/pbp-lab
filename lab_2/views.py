from django.http import response
from django.http.response import HttpResponse
from django.core import serializers
from django.shortcuts import render
from lab_2.models import Note

# Create your views here.

# untuk_siapa = 'Rhoma'
# dari_siapa = 'Ani'
# judul = 'Cukup'
# pesan = 'Cukup Rhoma!!! Cukup!!!'

# def index(request):
#     response = {'to': untuk_siapa,
#                 'from': dari_siapa,
#                 'title': judul,
#                 'message' : pesan}
#     return render(request, 'lab2.html', response)

def index(request):
    note = Note.objects.all()
    response = {'note' : note}
    return render(request, 'lab2.html', response)

def xml(request):
    # note = Note.objects.all()
    # response = {'note' : note}
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json(request):
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")



