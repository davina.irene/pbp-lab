1. Apakah perbedaan antara JSON dan XML?
- berdasarkan deskripsinya JSON (JavaScript Object Notation) merupakan standar terbuka berbasis teks untuk bertukar data, sedangkan XML (eXtensible Markup Language) merupakan format independen yang dimiliki perangkat lunak dan perangkat keras untuk bertukar data. 
- berdasarkan tipenya, JSON adalah bahasa meta, sedangkan XML adalah bahasa markup.
- berdasarkan kompleksitasnya, JSON tergolong lebih mudah dimengerti dan sederhana, sedangkan XML lebih rumit.
- berdasarkan orientasi, JSON berorientasi pada data, sedangkan XML berorientasi pada dokumen.
- JSON mendukung penggunaan array, sedangkan XML tidak.

2. Apakah perbedaan antara HTML dan XML?
- XML berfokus pada trasnfer data, sedangkan HTML berfokus pada penyajian data.
- XML menyediakan framework untuk menentukan bahasa markup, sedangkan HTML merupakan bahasa markup standar.
- XML memperhatikan penggunaan huruf besar dan kecil atau yang biasa disebut case sensitive, sedangkan HTML tidak.
- Tag pada XML biasanya dapat dikembangkan, sedangkan HTML tidak.
- Pada XML tidak ada toleransi untuk adanya error, sedangkan pada HTML error kecil dapat diabaikan.
- Pada XML harus menggunakan tag penutup, sedangkan HTML tidak harus.
- Informasi struktural pada XML sudah disediakan, sedangkan HTML tidak.