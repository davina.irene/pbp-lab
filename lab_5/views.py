from django.shortcuts import render
from django.http.response import HttpResponse
from django.core import serializers
from lab_4.forms import NoteForm

# Create your views here.
from lab_2.models import Note

def index(request):
    note = Note.objects.all()
    response = {'note' : note}
    return render(request, 'lab5_index.html', response)

def get_note(request):
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")

def update_note(request):
    # gangerti wkwkw
    context = {}
    form = NoteForm(request.POST or None)
    if(form.is_valid() and request.method == 'POST'):
        form.save()
        return redirect('/lab-4/')

    context['form'] = form
    return render(request, "lab4_form.html", context)