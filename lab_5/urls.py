from django.urls import path
from .views import get_note, index, note_list, update_note



urlpatterns = [
    path('', index, name='index'),
    path('/notes/<id>', get_note, name='get_note'),
    path('/notes/<id>/update', update_note, name='update_note')
]